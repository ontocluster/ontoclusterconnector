﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntoClusterConnector
{
    [Flags]
    public enum ValueType
    {
        None = 0,
        Id = 1,
        String = 2,
        Long = 4,
        Bool = 8,
        DateTime = 16,
        Double = 32,
        Exact = 64,
        GetAll = 128

    }
    public class QueryItem
    {
        public List<string> SpecialCharacters { get; set; } 
        public string Field { get; set; }
        public TimeSpan RangeForDateTime { get; set; }
        private object _originalValue;
        private object _value;
        public object Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _originalValue = value;
            }
        }

        public bool Exclude { get; set; }

        public string QueryPart
        {
            get
            {
                var queryPart = "";
                if (ValueType.HasFlag(ValueType.Id))
                {
                    queryPart = Field + ":" + _value.ToString();
                }
                else if (ValueType.HasFlag(ValueType.Long))
                {
                    queryPart = Field + ":" + _value.ToString();
                }
                else if (ValueType.HasFlag(ValueType.Bool))
                {
                    queryPart = Field + ":" + _value.ToString();
                }
                else if (ValueType.HasFlag(ValueType.Double))
                {
                    queryPart = Field + ":" + _value.ToString();
                }
                else if (ValueType.HasFlag(ValueType.DateTime))
                {
                    DateTime testDate;
                    if (_value is DateTime)
                    {
                        testDate = (DateTime) _value;
                        DateTime rangeEnd = testDate.Add(RangeForDateTime);
                        queryPart = Field + "[" + testDate.Year.ToString("0000") + "-" +
                                    testDate.Month.ToString("00") + "-" +
                                    testDate.Day.ToString("00") + "T" +
                                    testDate.Hour.ToString("00") + ":" +
                                    testDate.Hour.ToString("00") + " TO ";
                        queryPart += rangeEnd.Year.ToString("0000") + "-" +
                                     rangeEnd.Month.ToString("00") + "-" +
                                     rangeEnd.Day.ToString("00") + "T" +
                                     rangeEnd.Hour.ToString("00") + ":" +
                                     rangeEnd.Hour.ToString("00") + "]";
                    }
                    else
                    {
                        queryPart = Field + ":" + _value.ToString();
                    }
                }
                else if(ValueType.HasFlag(ValueType.String))
                {
                    string nameQuery = _value.ToString();
                    nameQuery = SpecialCharacters.Aggregate(nameQuery, (current, specialCharacter) => current.Replace(specialCharacter, "\\" + specialCharacter));
                    _value = nameQuery;
                    if (!ValueType.HasFlag(ValueType.Exact))
                    {
                        queryPart = Field + ":*" + _value.ToString() + "*";

                    }
                    else
                    {
                        queryPart += Field + ":" + _value.ToString();

                    }    
                }
                else if (ValueType.HasFlag(ValueType.GetAll))
                {
                    queryPart = Field + ":*";
                    if (Exclude)
                    {
                        queryPart = "(NOT " + queryPart + ")";
                    }
                    
                }
                

                return queryPart;
            }
        }
        public ValueType ValueType { get; set; }

        public QueryItem(List<string> specialCharacters)
        {
            SpecialCharacters = specialCharacters;
        }
    }
}
