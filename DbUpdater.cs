﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;
using OntoEntities;
using OntoEntities.DataClasses;

namespace OntoClusterConnector
{
    public class DbUpdater
    {
        private LogStates logStates = new LogStates();
        private DbSelector dbSelector;

        public DbUpdater(DbSelector dbSelector)
        {
            this.dbSelector = dbSelector;
        }

        public UpdateResultConnector SaveClusterConnectors(List<ClusterConnector> clusterConnectors)
        {
            dbSelector.ElConnector.Flush(dbSelector.Index);

            var result = new UpdateResultConnector
            {
                Result = logStates.Success,
                ToDoCount = clusterConnectors.Count
            };

            var objBulkDescriptors = new List<BulkDescriptor>();

            var count = 0;
            foreach (var clusterConnector in clusterConnectors)
            {
                BulkDescriptor bulkDescriptor;
                if (count == 0)
                {
                    bulkDescriptor = new BulkDescriptor();
                    objBulkDescriptors.Add(bulkDescriptor);
                }
                else
                {
                    bulkDescriptor = objBulkDescriptors.Last();
                }
                bulkDescriptor.Index<ClusterConnector>(
                            ix =>
                            ix.Id(clusterConnector.IdLeft + (!string.IsNullOrEmpty(clusterConnector.IdRight) ? clusterConnector.IdRight : "") + clusterConnector.IdRelation ).Document(clusterConnector).Type(nameof(ClusterConnector)));
                count++;

                if (count > 1000)
                {
                    count = 0;
                }

            }

            objBulkDescriptors.ForEach(bulkDescriptor =>
            {
                var bulkResult = dbSelector.ElConnector.Bulk(b => bulkDescriptor);
                var resultBulk = bulkResult.Items.Any(it => it.Error != null) ? logStates.Error : logStates.Success;

                if (resultBulk.Id == logStates.Error.Id)
                {
                    result.Result = resultBulk;
                }

                result.Result = bulkResult.Items.Any(it => it.Error != null) ? logStates.Error : logStates.Success;

            });

            if (result.Result.Id == logStates.Success.Id)
            {
                result.ResultList = clusterConnectors;
            }
            return result;
        }

        public UpdateResultEntity SaveClusterEntities(List<ClusterEntity> entities, EntityRole entityRole)
        {
            dbSelector.ElConnector.Flush(dbSelector.Index);

            var result = new UpdateResultEntity
            {
                Result = logStates.Success
            };

            //ToDo: Test Name

            if (entityRole == EntityRole.Class)
            {
                var classnameEntities = entities.Where(entity => entity.EntityRole == EntityRole.Class && string.IsNullOrEmpty(entity.Id)).Select(entity => new ClusterEntity { EntityRole = EntityRole.Class, Name = entity.Name }).ToList();
                if (classnameEntities.Any())
                {
                    var classNamesExistingDb = dbSelector.GetDataEntities(classnameEntities);
                    var classNameExisting = (from entity in entities
                                             join classNameEntity in classNamesExistingDb.ResultList on entity.Name equals
                                                 classNameEntity.Name
                                             where entity.Name.ToLower() == classNameEntity.Name.ToLower() &&
                                                 entity.Id != classNameEntity.Id
                                             select classNameEntity);

                    if (classNamesExistingDb.Result.Id == logStates.Error.Id)
                    {
                        result.Result = logStates.Error;
                        return result;
                    }

                    if (classNameExisting.Any())
                    {
                        result.ToDoCount = entities.Count;
                        result.DoneCount = 0;
                        result.ErrorCount = classNameExisting.Count();
                        result.Result = logStates.Relation;
                        return result;
                    }
                }

            }

            if (entityRole == EntityRole.RelationType)
            {
                var relationTypenameEntities = entities.Where(entity => entity.EntityRole == EntityRole.RelationType && string.IsNullOrEmpty(entity.Id)).Select(entity => new ClusterEntity { EntityRole = EntityRole.RelationType, Name = entity.Name }).ToList();
                if (relationTypenameEntities.Any())
                {
                    var relationTypeNamesExistingDb = dbSelector.GetDataEntities(relationTypenameEntities);
                    var relationTypeNameExisting = (from entity in entities
                                                    join relationTypeNameEntity in relationTypenameEntities on entity.Name equals
                                                        relationTypeNameEntity.Name
                                                    where entity.Name.ToLower() == relationTypeNameEntity.Name.ToLower() &&
                                                        entity.Id != relationTypeNameEntity.Id
                                                    select relationTypeNameEntity);

                    if (relationTypeNamesExistingDb.Result.Id == logStates.Error.Id)
                    {
                        result.Result = logStates.Error;
                        return result;
                    }

                    if (relationTypeNameExisting.Any())
                    {
                        result.ToDoCount = entities.Count;
                        result.DoneCount = 0;
                        result.ErrorCount = relationTypeNameExisting.Count();
                        result.Result = logStates.Relation;
                        return result;
                    }
                }



            }

            var objBulkDescriptors = new List<BulkDescriptor>();
            
            var count = 0;
            foreach (var clusterEntity in entities)
            {
                BulkDescriptor bulkDescriptor;
                if (count == 0)
                {
                    bulkDescriptor = new BulkDescriptor();
                    objBulkDescriptors.Add(bulkDescriptor);
                }
                else
                {
                    bulkDescriptor = objBulkDescriptors.Last();
                }
                bulkDescriptor.Index<ClusterEntity>(
                            ix =>
                            ix.Id(clusterEntity.Id).Document(clusterEntity).Type(nameof(ClusterEntity)));
                count++;

                if (count > 1000)
                {
                    count = 0;
                }

            }

            objBulkDescriptors.ForEach(bulkDescriptor =>
            {
                var bulkResult = dbSelector.ElConnector.Bulk(b => bulkDescriptor);
                var resultBulk = bulkResult.Items.Any(it => it.Error != null) ? logStates.Error : logStates.Success;

                if (resultBulk.Id == logStates.Error.Id)
                {
                    result.Result = resultBulk;
                }
              
                result.Result = bulkResult.Items.Any(it => it.Error != null) ? logStates.Error : logStates.Success;
                
            });
            
            if (result.Result.Id == logStates.Success.Id)
            {
                result.ResultList = entities;
            }
            return result;
        }
    }

    public class UpdateResultEntity
    {
        public ClusterEntity Result { get; set; }
        public List<ClusterEntity> ResultList { get; set; }

        public long ToDoCount { get; set; }
        public long DoneCount { get; set; }

        public long ErrorCount { get; set; }
    }

    public class UpdateResultConnector
    {
        public ClusterEntity Result { get; set; }
        public List<ClusterConnector> ResultList { get; set; }

        public long ToDoCount { get; set; }
        public long DoneCount { get; set; }

        public long ErrorCount { get; set; }
    }
}
