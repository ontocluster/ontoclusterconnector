﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nest;
using OntoEntities;
using OntoEntities.DataClasses;

namespace OntoClusterConnector
{
    public class DbSelector
    {
        public string Server { get; private set; }
        public int Port { get; private set; }
        public string Index { get; private set; }
        public string IndexRep { get; private set; }
        public int SearchRange { get; private set; }
        public string Session { get; private set; }
        public List<string> SpecialCharacters_Write { get; set; }
        public List<string> SpecialCharacters_Read { get; set; }

        public IElasticClient ElConnector { get; private set; }

        private LogStates logStates = new LogStates();

        public List<string> CreateClusterConnectorQuery(List<ClusterConnector> entityQuery)
        {
            var queryList = new List<string>();

            var queryRows = new List<QueryRow>();

            if (entityQuery != null)
            {
                foreach (var clusterConnector in entityQuery)
                {
                    var queryItems1 = new List<QueryItem>();
                    var queryItems2 = new List<QueryItem>();

                    if (!string.IsNullOrEmpty(clusterConnector.IdLeft))
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.IdLeft),
                            ValueType = ValueType.Id,
                            Value = clusterConnector.IdLeft
                        });
                    }

                    if (!string.IsNullOrEmpty(clusterConnector.IdParentLeft))
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.IdParentLeft),
                            ValueType = ValueType.Id,
                            Value = clusterConnector.IdParentLeft
                        });
                    }

                    if (!string.IsNullOrEmpty(clusterConnector.IdRight))
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.IdRight),
                            ValueType = ValueType.Id,
                            Value = clusterConnector.IdRight
                        });
                    }

                    if (!string.IsNullOrEmpty(clusterConnector.IdParentRight))
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.IdParentRight),
                            ValueType = ValueType.Id,
                            Value = clusterConnector.IdRight
                        });
                    }

                    if (!string.IsNullOrEmpty(clusterConnector.IdRelation))
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.IdRight),
                            ValueType = ValueType.Id,
                            Value = clusterConnector.IdRight
                        });
                    }

                    if (clusterConnector.ConnectorType != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.ConnectorType),
                            ValueType = ValueType.Long,
                            Value = (long)clusterConnector.ConnectorType
                        });
                    }

                    if (clusterConnector.EntityRoleRight != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.EntityRoleRight),
                            ValueType = ValueType.Long,
                            Value = (long)clusterConnector.EntityRoleRight
                        });
                    }

                    if (clusterConnector.EntityRoleLeft != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterConnector.EntityRoleLeft),
                            ValueType = ValueType.Long,
                            Value = (long)clusterConnector.EntityRoleLeft
                        });
                    }

                    if (clusterConnector.SearchHints != null)
                    {
                        foreach (var searchHint in clusterConnector.SearchHints)
                        {
                            queryItems2.Add(new QueryItem(SpecialCharacters_Read)
                            {
                                Field = nameof(clusterConnector.SearchHints),
                                ValueType = ValueType.String,
                                Value = searchHint
                            });
                        }

                    }

                    if (queryItems1.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems1,
                            ItemOperator = QueryOperator.AND
                        });
                    }

                    if (queryItems2.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems2,
                            ItemOperator = QueryOperator.AND
                        });
                    }
                }
            }

            if (queryRows.Any())
            {
                int ix = 0;
                int count = 0;
                while (ix < queryRows.Count)
                {
                    if (ix + 50 < queryRows.Count)
                    {
                        count = 50;
                    }
                    else
                    {
                        count = queryRows.Count - ix;
                    }

                    var queryListItem = string.Join(" " + Enum.GetName(typeof(QueryOperator), QueryOperator.OR) + " ",
                        queryRows.GetRange(ix, count).Select(queryRow => queryRow.RowQuery));

                    if (!queryList.Contains(queryListItem))
                    {
                        queryList.Add(queryListItem);
                    }


                    ix += count;
                }
            }
            else
            {
                queryList.Add("*");
            }


            queryRows.Clear();
            return queryList;
        }

        public List<string> CreateEntityQuery(List<ClusterEntity> entityQuery, bool boolClear = true)
        {
            var queryList = new List<string>();

            var queryRows = new List<QueryRow>();

            if (entityQuery != null)
            {
                foreach (var clusterEntity in entityQuery)
                {
                    var queryItems1 = new List<QueryItem>();
                    var queryItems2 = new List<QueryItem>();
                    var queryItems3 = new List<QueryItem>();
                    if (clusterEntity.EntityRole != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                           Field = nameof(clusterEntity.EntityRole),
                           ValueType = ValueType.Long,
                           Value = (long)clusterEntity.EntityRole
                        });
                    }

                    if (clusterEntity.BoolVal != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.EntityRole),
                            ValueType = ValueType.Bool,
                            Value = clusterEntity.BoolVal
                        });
                    }

                    if (clusterEntity.DoubleVal != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.DoubleVal),
                            ValueType = ValueType.Double,
                            Value = clusterEntity.DoubleVal
                        });
                    }

                    if (clusterEntity.DateTimeVal != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.DateTimeVal),
                            ValueType = ValueType.DateTime,
                            Value = clusterEntity.DateTimeVal
                        });
                    }

                    if (clusterEntity.Name != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.Name),
                            ValueType = ValueType.String,
                            Value = clusterEntity.Name
                        });
                    }

                    if (clusterEntity.Id != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.Id),
                            ValueType = ValueType.Id,
                            Value = clusterEntity.Id
                        });
                    }

                    if (clusterEntity.ParentIds != null)
                    {
                         
                        foreach (var parentId in clusterEntity.ParentIds)
                        {
                            queryItems2.Add(new QueryItem(SpecialCharacters_Read)
                            {
                                Field = nameof(clusterEntity.ParentIds),
                                ValueType = ValueType.String,
                                Value = parentId
                            });
                        }
                        
                    }
                    else if (clusterEntity.RootEntity)
                    {
                        queryItems2.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.ParentIds),
                            ValueType = ValueType.GetAll,
                            Exclude = true
                        });
                    }

                    if (clusterEntity.DataType != null)
                    {
                        queryItems1.Add(new QueryItem(SpecialCharacters_Read)
                        {
                            Field = nameof(clusterEntity.DataType),
                            ValueType = ValueType.Long,
                            Value = clusterEntity.DataType
                        });
                    }

                    if (clusterEntity.SearchHints != null)
                    {
                        foreach (var searchHint in clusterEntity.SearchHints)
                        {
                            queryItems3.Add(new QueryItem(SpecialCharacters_Read)
                            {
                                Field = nameof(clusterEntity.SearchHints),
                                ValueType = ValueType.String,
                                Value = searchHint
                            });
                        }

                    }

                    if (queryItems1.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems1,
                            ItemOperator = QueryOperator.AND
                        });
                    }

                    if (queryItems2.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems2,
                            ItemOperator = QueryOperator.AND
                        });
                    }

                    if (queryItems3.Any())
                    {

                        queryRows.Add(new QueryRow
                        {
                            QueryItems = queryItems3,
                            ItemOperator = QueryOperator.AND
                        });
                    }
                }

                
            }

            
            if (queryRows.Any())
            {
                int ix = 0;
                int count = 0;
                while (ix < queryRows.Count)
                {
                    if (ix + 50 < queryRows.Count)
                    {
                        count = 50;
                    }
                    else
                    {
                        count = queryRows.Count - ix;
                    }

                    var queryListItem = string.Join(" " + Enum.GetName(typeof(QueryOperator), QueryOperator.OR) + " ",
                        queryRows.GetRange(ix, count).Select(queryRow => queryRow.RowQuery));

                    if (!queryList.Contains(queryListItem))
                    {
                        queryList.Add(queryListItem);
                    }
                    

                    ix += count;
                }
            }
            else
            {
                queryList.Add("*");
            }


            queryRows.Clear();
            return queryList;
        }

        public DbSelector(string server,
                          int port,
                          string index,
                          string indexRep,
                          int searchRange,
                          string session)
        {
            this.Server = server;
            this.Port = port;
            this.Index = index;
            this.IndexRep = indexRep;
            this.SearchRange = searchRange;
            this.Session = session;

            SpecialCharacters_Read = new List<string> { "\\", "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":", "@", "/" };
            //SpecialCharacters_Write = new List<string> { ":", "\"" };
            //SpecialCharacters_Read = new List<string> { " ", ":", "/", "\"" };
            initialize_Client();
            




        }

        public SelectResultConnector GetDataConnectors(ConnectorType connectorType, List<ClusterConnector> connectorFilter = null, bool onlyIds = false)
        {
            SelectResultConnector resultConnectors = new SelectResultConnector
            {
                Result = logStates.Success,
                ResultList = new List<ClusterConnector>()
            };

            var queries = CreateClusterConnectorQuery(connectorFilter);
            foreach (var query in queries)
            {
                var intCount = SearchRange;
                var intPos = 0;

                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<ClusterConnector>(Indices.Index(Index), Types.Type(nameof(ClusterConnector)));
                searchRequest.Query = queryItem;
                searchRequest.From = intPos;
                searchRequest.Size = SearchRange;
                searchRequest.Scroll = "10s";

                string scrollId = null;

                while (intCount > 0)
                {
                    intCount = 0;

                    ISearchResponse<ClusterConnector> searchResult = null;
                    if (scrollId == null)
                    {


                        searchResult = ElConnector.Search<ClusterConnector>(searchRequest);
                        scrollId = searchResult.ScrollId;


                    }
                    else
                    {
                        searchResult = ElConnector.Scroll<ClusterConnector>("10s", scrollId);
                    }


                    if (connectorType == ConnectorType.Full)
                    {
                        resultConnectors.ResultList.AddRange(from doc in searchResult.Documents
                                                             join oldDoc in resultConnectors.ResultList on new { IdLeft = doc.IdLeft, IdRight = doc.IdRight, IdRelation = doc.IdRelation } equals 
                                                                                                           new { IdLeft = oldDoc.IdLeft, IdRight = oldDoc.IdRight, IdRelation = oldDoc.IdRelation } into oldDocs
                                                             from oldDoc in oldDocs.DefaultIfEmpty()
                                                             where oldDoc == null
                                                             select doc);
                    }
                    else
                    {
                        resultConnectors.ResultList.AddRange(from doc in searchResult.Documents
                                                             join oldDoc in resultConnectors.ResultList on new { IdLeft = doc.IdLeft, IdRelation = doc.IdRelation } equals
                                                                                                           new { IdLeft = oldDoc.IdLeft, IdRelation = oldDoc.IdRelation } into oldDocs
                                                             from oldDoc in oldDocs.DefaultIfEmpty()
                                                             where oldDoc == null
                                                             select doc);

                    }
                    

                    if (searchResult.Documents.Count() < SearchRange)
                    {
                        intCount = 0;
                    }
                    else
                    {
                        intCount = searchResult.Documents.Count();
                        intPos += SearchRange;
                    }

                }
            }

            return resultConnectors;
        }

        public SelectResultEntity GetDataEntities(List<ClusterEntity> entityFilter = null, bool rootEntity = false)
        {
            SelectResultEntity resltEntities = new SelectResultEntity
            {
                Result = logStates.Success,
                ResultList =  new List<ClusterEntity>()
            };

            var queries = CreateEntityQuery(entityFilter);
            foreach (var query in queries)
            {
                var intCount = SearchRange;
                var intPos = 0;

                var queryItem = new QueryStringQuery();
                queryItem.Query = query;

                var searchRequest = new SearchRequest<ClusterEntity>(Indices.Index(Index), Types.Type(nameof(ClusterEntity)));
                searchRequest.Query = queryItem;
                searchRequest.From = intPos;
                searchRequest.Size = SearchRange;
                searchRequest.Scroll = "10s";

                string scrollId = null;


                while (intCount > 0)
                {
                    intCount = 0;

                    ISearchResponse<ClusterEntity> searchResult = null;
                    if (scrollId == null)
                    {
                        
                        
                        searchResult = ElConnector.Search<ClusterEntity>(searchRequest);
                        scrollId = searchResult.ScrollId;
                        

                    }
                    else
                    {
                        searchResult = ElConnector.Scroll<ClusterEntity>("10s", scrollId);
                    }


                    resltEntities.ResultList.AddRange(from doc in searchResult.Documents
                                                       join oldDoc in resltEntities.ResultList on doc.Id equals oldDoc.Id into oldDocs
                                                       from oldDoc in oldDocs.DefaultIfEmpty()
                                                       where oldDoc == null
                                                       select doc);

                    if (searchResult.Documents.Count() < SearchRange)
                    {
                        intCount = 0;
                    }
                    else
                    {
                        intCount = searchResult.Documents.Count();
                        intPos += SearchRange;
                    }








                }
            }

            return resltEntities;

        }

        public List<string> IndexList(string server, int port)
        {
            var uri = new Uri("http://" + server + ":" + port.ToString());

            var settings = new ConnectionSettings(uri);
            var objElConnector = new ElasticClient(settings);
            var dictIndex = objElConnector.CatIndices(q => q.AllIndices());

            return dictIndex.Records.Select(rec => rec.Index).ToList();
        }

        private void initialize_Client()
        {
            var uri = new Uri("http://" + Server + ":" + Port.ToString());//.Purify();

            var settings = new ConnectionSettings(uri).DefaultIndex(Index);
            settings.DefaultTypeNameInferrer(i => i.Name);
            settings.DefaultFieldNameInferrer(p => p);
            ElConnector = new ElasticClient(settings);
            try
            {
                var indexList = IndexList(Server, Port);
                if (indexList.All(ix => ix != IndexRep))
                {
                    try
                    {
                        ElConnector.CreateIndex(IndexRep);

                    }
                    catch (Exception ex)
                    {

                        throw new Exception("Report index!");
                    }
                }
            }
            catch (Exception)
            {

                throw new Exception("Indexes not found!");
            }


        }
    }

    public class SelectResultEntity
    {
        public ClusterEntity Result { get; set; }
        public List<ClusterEntity> ResultList { get; set; }
    }

    public class SelectResultConnector
    {
        public ClusterEntity Result { get; set; }
        public List<ClusterConnector> ResultList { get; set; }
    }
}
